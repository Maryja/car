<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Car expenses</title>

        <link rel="stylesheet" href="{{ asset('css/my.css') }}">
    </head>
    <body>
    <div class="container">
        <div class="container__item landing-page-container">
            <div class="content-wrapper">

                <header class="header">

                    @if (Route::has('login'))
                        <div class="top-right links">
                            @auth
                                <a class="home-link" href="{{ url('/home') }}">Mano paskyra</a>
                            @else
                                <a class="button" href="{{ route('login') }}">Prisijungti</a>
                                <a class="button" href="{{ route('register') }}">Registruotis</a>
                            @endauth
                        </div>
                    @endif


                </header>


                <div class="container home-container">
                    <div class="row justify-content-center">
                        <div class="col-md-8">
                            @if (session('status'))
                                <div class="alert alert-success">
                                    {{ session('status') }}
                                </div>
                            @endif

                            <div class="my-container">
                                <span class="text1">Kiek idėdi į automobilį?</span>
                                <span class="text2">Paskaičiuok čia!</span>
                            </div>

                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>




    </body>
</html>

