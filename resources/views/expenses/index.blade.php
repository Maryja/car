
@extends('layouts.app')

@section('content')
    <div class="container home-container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif

                    <div class="card" style="width: 100%;">
                        <div class="card-header">
                            Išlaidos :
                        </div>

                        <ul class="list-group list-group-flush">
                    @foreach($expenses as $exp)


                                <li class="list-group-item color-item">Išlaidų data: {{ $exp->created_at}}€</li>
                                <li class="list-group-item">Draudimas : {{ $exp->insurance}}€</li>
                                <li class="list-group-item">Kuras : {{ $exp->fuel}}€</li>
                                <li class="list-group-item">Parkavimas: {{ $exp->parking}}€</li>
                                <li class="list-group-item">Remontas: {{ $exp->repair}}€</li>
                                <li class="list-group-item">Baudos: {{ $exp->fine}}€</li>
                                <li class="list-group-item">Tiuningas: {{ $exp->tuning}}€</li>
                                <li class="list-group-item">Techninė apžiūra: {{ $exp->technical_inspection}}€</li>
                                <li class="list-group-item">Padangos: {{ $exp->tires}}€</li>
                                <li class="list-group-item">Kitos išlaidos: {{ $exp->other}}€</li>







                    @endforeach
                        </ul>
                    </div>


            </div>
        </div>
    </div>
@endsection