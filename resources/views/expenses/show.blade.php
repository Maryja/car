@extends('layouts.app')

@section('content')
    <div class="container home-container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif



                        @foreach($expenses as $file)

                        <div class="card" style="width: 18rem;">
                            <div class="card-header">
                                Išlaidos :
                            </div>
                            <ul class="list-group list-group-flush">
                                <li class="list-group-item">Išlaidų data: {{ $file->created_at}}</li>
                                <li class="list-group-item">Draudimas : {{ $file->insurance}}</li>
                                <li class="list-group-item">Kuras : {{ $file->fuel}}</li>
                                <li class="list-group-item">Parkavimas: {{ $file->parking}}</li>
                                <li class="list-group-item">Remontas: {{ $file->repair}}</li>
                                <li class="list-group-item">Baudos: {{ $file->fine}}</li>
                                <li class="list-group-item">Tiuningas: {{ $file->tuning}}</li>
                                <li class="list-group-item">Techninė apžiūra: {{ $file->technical_inspection}}</li>
                                <li class="list-group-item">Padangos: {{ $file->tires}}</li>
                                <li class="list-group-item">Kitos išlaidos: {{ $file->other}}</li>
                            </ul>
                        </div>


                        @endforeach




            </div>
        </div>
    </div>
@endsection
