@extends('layouts.app')

@section('content')
    <div class="container home-container">
        <div class="row justify-content-center">
            <div class="col-md-8">

                <form method="post" action="{{ route('expenses.store') }}">
@csrf

                    <div class="col">
                        <label for="carBrand">Automobilio markė</label>
                        <select class="form-control" name="carBrand" id="carBrand"  required>



                        @foreach($cars as $car)
                            <option value="{{ $car->id }}">{{ $car->name }}</option>
                        @endforeach





                        </select>
                    </div>
                    <h2>Išlaidos:</h2>
<div class="row">

                    <div class="col">
                        <label for="insurance">Draudimas</label>
                        <input type="number" name="insurance" class="form-control" id="insurance" value="0">
                    </div>

                    <div class="col">
                        <label for="fuel">Kuras</label>
                        <input type="number" name="fuel" class="form-control" id="fuel" value="0">
                    </div>

                    <div class="col">
                        <label for="parking">Parkavimas</label>
                        <input type="number" name="parking" class="form-control" id="parking" value="0">
                    </div>
</div>

                    <div class="row">
                    <div class="col">
                        <label for="repair">Remontas</label>
                        <input type="number"name="repair" class="form-control" id="repair" value="0">
                    </div>

                    <div class="col">
                        <label for="fine">Baudos</label>
                        <input type="number" name="fine" class="form-control" id="fine" value="0">
                    </div>


                    <div class="col">
                        <label for="tuning">"Tiuningas"</label>
                        <input type="number" name="tuning" class="form-control" id="tuning" value="0">
                    </div>
                    </div>

                    <div class="row">
                    <div class="col">
                        <label for="ta">T.a.</label>
                        <input type="number" name="ta" class="form-control" id="ta" value="0">
                    </div>

                    <div class="col">
                        <label for="tires">Padangos</label>
                        <input type="number" name="tires" class="form-control" id="tires" value="0">
                    </div>

                    <div class="col">
                        <label for="other">Kitos</label>
                        <input type="number" name="other" class="form-control" id="other" value="0">
                    </div>
                </div>
                    <button type="submit" class="btn btn-primary">submit</button>
                </form>



        </div>
    </div>
@endsection
