@extends('layouts.app')

@section('content')


    <div class="container home-container">
        <div class="row justify-content-center">
            <div class="col-md-8">
@auth
                <form method="post" action="{{ route('cars.store') }}">
                    @csrf
                    <meta name="csrf-token" content="{{ csrf_token() }}">



                    <div class="form-group">
                        <label for="carBrand">Automobilio markė</label>
                        <select class="form-control" name="carBrand" id="brand" >

                            <option value="0" disabled = "true" selected = "true">=== Pasirinkite gamintoją ===</option>
                            @foreach($brand as $br)
                                <option value="{{ $br->id }}">{{ $br->name }}</option>
                            @endforeach


                        </select>
                    </div>

                    <div class="form-group">
                        <label for="carModel">Automobilio modelis</label>
                        <select class="form-control" name="carModel" id="model">

                            <option value="0" disabled = "true" selected = "true">=== Pasirinkite modelį ===</option>



                        </select>
                    </div>


                    <div class="form-group">
                        <label for="manufactureYear">Pagaminimo metai</label>
                        <input type="number" class="form-control" name="manufactureYear" id="manufactureYear">
                    </div>

                    <div class="form-group">
                        <label for="purchaseYear">Pirkimo metai</label>
                        <input type="number" class="form-control" name="purchaseYear" id="purchaseYear">
                    </div>

                    <div class="form-group">
                        <label for="price">Pirkimo kaina</label>
                        <input type="number" class="form-control" name="price" id="price">
                    </div>
                    <button type="submit" class="btn btn-primary">submit</button>
                </form>

@endauth

            </div>
        </div>
    </div>





    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>




    <script>
        // alert( 'scriptas pasileido' );

        $(function() {
            $.ajaxSetup({

                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

                }

            });

            $('#brand').on('change', function (e) {
                let id = this.selectedOptions[0].value;
                let carModel = document.getElementById('model');
                $.get(`/carextends/public/models/${id}`, (response) =>{
                    carModel.innerHTML = '<option value="0" disabled = "true" selected = "true">=== Pasirinkite modelį ===</option>';
                    $.each(response, (key, value)=>{

                        var option = document.createElement("option");
                        option.text = value.name;
                        option.value = value.id;
                        carModel.appendChild(option)
                    });
                });
            });
        })

    </script>



























@endsection
