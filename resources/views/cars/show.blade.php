@extends('layouts.app')

@section('content')
    <div class="container home-container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif


<ul>

@foreach($brand as $br)
    <li>{{ $br->name}} </li>
@endforeach

</ul>

            </div>
        </div>
    </div>
@endsection
