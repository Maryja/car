@extends('layouts.app')

@section('content')
    <div class="container home-container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif


                    <div class="list-group">
                        <a href="#" class="list-group-item list-group-item-action active my-own-color-list-group">
                            Mano mašinos:
                        </a>

                    @foreach($cars as $car)


                            <a href="#" class="list-group-item list-group-item-action">{{ $car -> name }}  {{ $car -> manufacture_year }}m. </a>


                    @endforeach
                    </div>

            </div>
        </div>
    </div>
@endsection
