@extends('layouts.app')

@section('content')
<div class="container home-container">
    <div class="row justify-content-center">
        <div class="col-md-8">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
@guest
<div class="my-container">
<span class="text1">Kiek idėdi į automobilį?</span>
            <span class="text2">Paskaičiuok čia!</span>
</div>
@endguest

            @auth

                <div class="my-flex-container">

                            <a href="{{ route('cars.index') }}">
                <div class="cars">
                    <img src="{{ asset('css/img/mycar.png') }}" alt="car">
                    <h3>Mano mašinos</h3>
                </div>
                            </a>
                            <a href="{{ route('expenses.index') }}">
                                <div class="cars">
                                    <img src="{{ asset('css/img/euro.png') }}" alt="car">
                                    <h3>Visos išlaidos</h3>
                                </div>
                            </a>


                </div>
                @endauth
        </div>
    </div>
</div>
@endsection
