<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    public function expensesObject(){
        return $this->hasMany('App\Expenses');
    }

    public function userObject(){
        return $this->belongsTo('App\User');
    }

    public function brandObject(){
        return $this->hasMany('App\Brand');
    }

    public function modelsObject(){
        return $this->hasMany('App\Models');
    }
}
