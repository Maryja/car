<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Models extends Model
{
    protected $table = 'models';

    public function brandObject(){
        return $this->belongsTo('App\Brand');
    }
}
