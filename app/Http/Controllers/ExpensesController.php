<?php

namespace App\Http\Controllers;

use App\Brand;
use App\Car;
use App\Expenses;
use App\Models;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ExpensesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $expenses = DB::table('expenses')->join('cars', function($join)
        {
            $join->on('expenses.cars_id', '=', 'cars.id')->where('cars.user_id', '=',  Auth::user()-> id);
        })->get();
        return view('expenses.index', ['expenses' => $expenses]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cars = DB::table('cars')->join('models', function($join)
        {
            $join->on('cars.models_id', '=', 'models.id')->where('cars.user_id', '=',  Auth::user()-> id);
        })->get();

        return view('expenses.insert',['cars' => $cars]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $expenses = new Expenses();
        $expenses->cars_id = $data['carBrand'];
        $expenses->insurance = $data['insurance'];
        $expenses->fuel = $data['fuel'];
        $expenses->parking = $data['parking'];
        $expenses->repair = $data['repair'];
        $expenses->fine = $data['fine'];
        $expenses->tuning = $data['tuning'];
        $expenses->technical_inspection = $data['ta'];
        $expenses->tires = $data['tires'];
        $expenses->other = $data['other'];
        $expenses->save();
        return  view('home');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
//        $cars_id = Car::where('user_id', '=', $id)->get();
//
//
//        $expenses = Expenses::whereIn ('cars_id', $cars_id)->get();
//
//
//        return  view('expenses.show ',['expenses' => $expenses]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
