<?php

namespace App\Http\Controllers;

use App\Brand;
use App\Car;
use App\Models;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CarController extends Controller
{
    public function index()
    {
        $cars = DB::table('cars')->join('brands', function($join)
        {
            $join->on('cars.brand_id', '=', 'brands.id')->where('cars.user_id', '=',  Auth::user()-> id);
        })->get();


        return view('cars.index', ['cars' => $cars]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $brands = Brand::all();
        $model = Models::all();


       return view('cars.insert',['brand'=>$brands,'model'=>$model]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $cars = new Car();
        $cars->user_id = Auth::user()->id;
        $cars->brand_id = $data['carBrand'][0];
        $cars->manufacture_year = $data['manufactureYear'];
        $cars->purchase_year = $data['purchaseYear'];
        $cars->price = $data['price'];
        $cars->models_id = $data['carModel'][0];
        $cars->save();
        return redirect(route('home'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
















}
